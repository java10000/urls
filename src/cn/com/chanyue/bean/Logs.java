package cn.com.chanyue.bean;

import org.nutz.dao.entity.annotation.*;

/**
 * 
 * @author Howe
 *
 */
@Table("tb_chanyue_url_logs")
public class Logs {

	/**
	 * 短Key
	 */
	@Column("shortKey")
	private String shortKey;
	
	public String getShortKey() {
		return shortKey;
	}

	public void setShortKey(String shortKey) {
		this.shortKey = shortKey;
	}
	/**
	 * 访问IP
	 */
	@Column("accessIp")
	private String accessIp;
	
	public String getAccessIp() {
		return accessIp;
	}

	public void setAccessIp(String accessIp) {
		this.accessIp = accessIp;
	}
	/**
	 * 访问时间
	 */
	@Column("accessTime")
	private java.util.Date accessTime;
	
	public java.util.Date getAccessTime() {
		return accessTime;
	}

	public void setAccessTime(java.util.Date accessTime) {
		this.accessTime = accessTime;
	}
	/**
	 * 访问机器
	 */
	@Column("accessAgent")
	private String accessAgent;
	
	public String getAccessAgent() {
		return accessAgent;
	}

	public void setAccessAgent(String accessAgent) {
		this.accessAgent = accessAgent;
	}
	/**
	 * 跳转来源
	 */
	@Column("referer")
	private String referer;
	
	public String getReferer() {
		return referer;
	}

	public void setReferer(String referer) {
		this.referer = referer;
	}
}