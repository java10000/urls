var ioc = {
	dataSource : {
		type :"com.alibaba.druid.pool.DruidDataSource",
		events : {
			depose :"close"
		},
		fields : {
			driverClassName : 'com.mysql.jdbc.Driver',
			url             : 'jdbc:mysql://localhost:3306/urls?useUnicode=true&characterEncoding=UTF-8',
			username        : 'root',
			password        : 'cBehbPS3LSQKqR/trOB2nv5q0DC85mQ0xcCi5hljesSIL0USjpiRphC0wkPoQX0Z45xtRwirwsE/fzxcUClYAQ==',
			initialSize     : 1,
			maxActive       : 50,
			minIdle         : 1,
			filters		    : 'config',
			connectionProperties : 'config.decrypt=true',
			defaultAutoCommit: false,
			
			//validationQueryTimeout : 5,
			validationQuery : "select 1"
		}
	},
    dao : {
        type : "org.nutz.dao.impl.NutDao",
        fields : {
        	dataSource : {refer : 'dataSource'}
        }
    }
};